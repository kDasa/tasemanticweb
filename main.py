import showTable
from flask import Flask, request, Markup, render_template
from flask_restful import Resource, Api
import codecs

app = Flask(__name__, static_url_path='/static')
api = Api(app)

@app.route('/')
def start():
	agent	= request.headers.get('User-Agent')
	phones	= ["iphone", "android", "blackberry"]
	if any(phone in agent.lower() for phone in phones):
		return render_template("indexMobile.html")
	else:
		return render_template("index.html")

@app.route('/kalimantan')
def kalim():
	return render_template("kalimantan.html")

@app.route('/sumatera')
def sumat():
	return render_template("sumatera.html")

@app.route('/sulawesi')
def sulaw():
	return render_template("sulawesi.html")

@app.route('/maluku_papua')
def maluk():
	return render_template("maluku_papua.html")

@app.route('/jawa')
def jawa():
	return render_template("jawa.html")

@app.route('/bali_nusa_tenggara')
def balin():
	return render_template("bali_nusa_tenggara.html")

@app.route('/data/<daerah>')
def data(daerah):
    table = cariMakan(daerah)
    return render_template("display.html", table=table, daerah=str(daerah))
	
@app.route('/<daerah>')
def cariMakan(daerah):
	agent	= request.headers.get('User-Agent')
	phones	= ["iphone", "android", "blackberry"]
	if any(phone in agent.lower() for phone in phones):
		makanan = showTable.getFoodMobile(daerah)
	else:
		makanan = showTable.getFood(daerah)
	return makanan

@app.route('/search')
def cari():
	return render_template("search2.html")
	
@app.route('/search2')
def cari2():
	return render_template("search.html")

@app.route('/handle_data', methods=['POST'])
def handle_data():
    daerah = request.form['daerah']
    table = cariMakan(daerah)
    return render_template("display.html", table=table, daerah=str(daerah))


if __name__ =='__main__':
   app.run()