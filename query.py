from rdflib import Graph
from SPARQLWrapper import SPARQLWrapper, RDF

def getGraph():
	g = Graph()
	g.parse("budaya.ttl", format="ttl")

	sparql1 = SPARQLWrapper("http://id.dbpedia.org/sparql")
	query1 = """
		PREFIX dbpedia: <http://id.dbpedia.org/resource/> 
		PREFIX bkbr: 	<https://budayakb.cs.ui.ac.id/resource/>
		PREFIX kat: 	<http://id.dbpedia.org/resource/Kategori:>
		PREFIX foaf:	<http://xmlns.com/foaf/0.1/>
		CONSTRUCT 	
		{
			?Food dbpedia-owl:abstract 	?Abstract.
			?Food dbpedia-owl:thumbnail	?Thumbnail.
			?Food foaf:isPrimaryTopicOf	?wiki
		}
		WHERE		
		{
			?Food dbpedia-owl:abstract 	?Abstract.
			?Food dbpedia-owl:thumbnail	?Thumbnail.
			?Food dcterms:subject		kat:Masakan_Indonesia.
			?Food foaf:isPrimaryTopicOf	?wiki
		}
	"""
	sparql1.setQuery(query1)
	sparql1.setReturnFormat(RDF)
	results1 = sparql1.query()
	triples1 = results1.convert()
	g += triples1



	sparql = SPARQLWrapper("https://query.wikidata.org/sparql")
	query = """
		CONSTRUCT 
		{
			?item rdfs:label ?itemLabel.
			?item wdt:P186  ?material.
			?material rdfs:label ?materialLabel
		}
		WHERE
		{
			?item wdt:P31   wd:Q2095.
			?item wdt:P17   wd:Q252.
			?item wdt:P186  ?material.
			?item rdfs:label ?item_label filter (lang(?item_label) = "id").
			?material rdfs:label ?material_label filter (lang(?material_label) = "id").
			SERVICE wikibase:label { bd:serviceParam wikibase:language "id,en" }
		}
	"""
	sparql.setQuery(query)
	sparql.setReturnFormat(RDF)
	results = sparql.query()
	triples = results.convert()
	g += triples
	
	return g