from rdflib import Graph, Namespace
from flask_table import Table, Col
import query

bkb 		= Namespace("https://budayakb.cs.ui.ac.id/ns#")
bkbr 		= Namespace("https://budayakb.cs.ui.ac.id/resource/")
dbpedia 	= Namespace("http://id.dbpedia.org/resource/")
dbpediaowl 	= Namespace("http://dbpedia.org/ontology/")
wdt			= Namespace("http://www.wikidata.org/prop/direct/")
rdfs		= Namespace("http://www.w3.org/2000/01/rdf-schema#")
foaf		= Namespace("http://xmlns.com/foaf/0.1/")



graph = query.getGraph()

def getFood(daerah):
	
	class ItemTable(Table):
		classes = ["table", "table-hover"]
		name = Col('Name')
		pict = Col('Image')
		ingredients = Col('Ingredient(s)')
		description = Col('Decription')
	
	class Item(object):
		def __init__(self, name, pict, ingredients, description):
			self.name = name
			self.pict = pict
			self.ingredients = ingredients
			self.description = description
	
	global graph
	foods = []
	daerah = "https://budayakb.cs.ui.ac.id/resource/" + daerah
	for s,p,o in graph.triples( (None, None, bkb.MakananMinumanTradisional) ):
	
		for a,b,c in graph.triples( (s, bkb.originFromProvince, None) ):
		
			if str(c) == daerah:			
				a = str(a)
				a = a.replace('https://budayakb.cs.ui.ac.id/resource/','')
				foods.append(a)
			
		for d,e,f in graph.triples( (s, bkb.originFromCity, None) ):
		
			if str(f) == daerah:			
				d = str(d)
				d = d.replace('https://budayakb.cs.ui.ac.id/resource/','')
				foods.append(d)
				
	#return str(foods)
	
	daerah = daerah.replace('https://budayakb.cs.ui.ac.id/resource/','')
	
	listMakanan = []
	table = ""
	
	for food in foods:
		listBahan	= []
		wikilink = ""
		for a,b,c in graph.triples( (None, rdfs.label, None) ):
			if str(c) ==food:
				for d,e,f in graph.triples( (a, wdt.P186, None) ):
					for x,y,z in graph.triples( (f, rdfs.label, None ) ):
						listBahan.append(str(z))
				
		for x,y,z in graph.triples( (None, dbpediaowl.abstract, None) ):
			if food == str(x).replace('http://id.dbpedia.org/resource/',''):
				
				for m,n,o in graph.triples( (x, foaf.isPrimaryTopicOf, None) ):
					wikilink = str(o)
				for u,v,w in graph.triples( (x, dbpediaowl.thumbnail, None) ):
					w = str(w).replace('http://dbpedia.org/ontology/', '')
					w = '<img src='+w+' alt="No Results"></img>'
					x = str(x).replace('http://id.dbpedia.org/resource/','')
					x = '<a href='+wikilink+'>'+x+'</a>'
					if listBahan == []:
						listBahan = "No Results"
					else:
						listBahan = '%s' % ', '.join(map(str, listBahan))
					listMakanan.append(Item(x, w, str(listBahan), str(z)))
	
	table = ItemTable(listMakanan, border = '1px solid black')
	
	if foods == [] or listMakanan == []:
		return "No Result"
	else:
		#return listMakanan
		table = table.__html__()
		table = table.replace('&lt;','<')
		table = table.replace('&gt;','>')
	return (table)
	
def getFoodMobile(daerah):

	class ItemTableMobile(Table):
		classes = ["text-center"]
		content = Col('Culinary Item')
		
	class ItemMobile(object):
		def __init__(self, content):
			self.content = content
	
	global graph
	table = ""
	foods = []
	daerah = "https://budayakb.cs.ui.ac.id/resource/" + daerah
	for s,p,o in graph.triples( (None, None, bkb.MakananMinumanTradisional) ):
	
		for a,b,c in graph.triples( (s, bkb.originFromProvince, None) ):
		
			if str(c) == daerah:			
				a = str(a)
				a = a.replace('https://budayakb.cs.ui.ac.id/resource/','')
				foods.append(a)
			
		for d,e,f in graph.triples( (s, bkb.originFromCity, None) ):
		
			if str(f) == daerah:			
				d = str(d)
				d = d.replace('https://budayakb.cs.ui.ac.id/resource/','')
				foods.append(d)
				
	#return str(foods)
	
	daerah = daerah.replace('https://budayakb.cs.ui.ac.id/resource/','')
	
	listMakanan = []
	
	for food in foods:
		listBahan	= []
		tableMobile = ""
		wikilink = ""
		
		for a,b,c in graph.triples( (None, rdfs.label, None) ):
			if str(c) ==food:
				for d,e,f in graph.triples( (a, wdt.P186, None) ):
					for x,y,z in graph.triples( (f, rdfs.label, None ) ):
						listBahan.append(str(z))				
				
		for x,y,z in graph.triples( (None, dbpediaowl.abstract, None) ):
			if food == str(x).replace('http://id.dbpedia.org/resource/',''):
				for m,n,o in graph.triples( (x, foaf.isPrimaryTopicOf, None) ):
					wikilink = str(o)
				for u,v,w in graph.triples( (x, dbpediaowl.thumbnail, None) ):
					w = str(w).replace('http://dbpedia.org/ontology/', '')
					w = '<img src='+w+' alt=No_Results></img>'
					x = str(x).replace('http://id.dbpedia.org/resource/','')
					x = '<a href='+wikilink+'>'+x+'</a>'
					if listBahan == []:
						listBahan = "No Results"
					else:
						listBahan = '%s' % ', '.join(map(str, listBahan))
					listMakanan.append(ItemMobile(x))
					listMakanan.append(ItemMobile(w))
					listMakanan.append(ItemMobile(str(listBahan)))
					listMakanan.append(ItemMobile(str(z)))
					tableMobile = ItemTableMobile(listMakanan, border = '1px solid black')
					listMakanan = []
					table += tableMobile.__html__() +"<br/>"
	
	if foods == [] or table == "":
		return "No Result"
	else:
		table = table.replace('&lt;','<')
		table = table.replace('&gt;','>')
	return (table)
